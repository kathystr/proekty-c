
// lab7Dlg.cpp: файл реализации
//
#include <string>
#include <iostream>
#include "stdafx.h"
#include "lab7.h"
#include "lab7Dlg.h"
#include "afxdialogex.h"
#include <fstream>
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
using namespace std;

// Диалоговое окно CAboutDlg используется для описания сведений о приложении

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV

// Реализация
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Диалоговое окно Clab7Dlg



Clab7Dlg::Clab7Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_LAB7_DIALOG, pParent)
	, name_file_in(_T(""))
	, name_file_out(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Clab7Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_IN, name_file_in);
	DDX_Text(pDX, IDC_EDIT_FILEOUT, name_file_out);
}

BEGIN_MESSAGE_MAP(Clab7Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_INFILE, &Clab7Dlg::OnBnClickedBtnInFile)
	ON_EN_CHANGE(IDC_EDIT_FILEOUT, &Clab7Dlg::OnEnChangeEdit2)
	ON_EN_CHANGE(IDC_EDIT_FILEOUT, &Clab7Dlg::OnEnChangeEditFileout)
	//ON_BN_CLICKED(IDC_BUTTON2, &Clab7Dlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BTN_OUT, &Clab7Dlg::OnBnClickedBtnOut)
	ON_BN_CLICKED(IDC_BTN_DOIT, &Clab7Dlg::OnBnClickedBtnDoit)
END_MESSAGE_MAP()


// Обработчики сообщений Clab7Dlg

BOOL Clab7Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Добавление пункта "О программе..." в системное меню.

	// IDM_ABOUTBOX должен быть в пределах системной команды.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию
	name_file_in = "in.txt";
	UpdateData(0);
	name_file_out = "out.txt";
	UpdateData(0);
	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

void Clab7Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void Clab7Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR Clab7Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void Clab7Dlg::OnBnClickedBtnInFile()
{
	// TODO: добавьте свой код обработчика уведомлений
	CFileDialog d(1);//диалог открытия файла
	if (d.DoModal() == IDOK) {
		name_file_in = d.m_ofn.lpstrFile;
		UpdateData(0);
	}

}


void Clab7Dlg::OnEnChangeEdit2()
{
	// TODO:  Если это элемент управления RICHEDIT, то элемент управления не будет
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// функция и вызов CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Добавьте код элемента управления
}


void Clab7Dlg::OnEnChangeEditFileout()
{
	// TODO:  Если это элемент управления RICHEDIT, то элемент управления не будет
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// функция и вызов CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Добавьте код элемента управления
}


void Clab7Dlg::OnBnClickedButton2()
{
	CFileDialog d(0);//диалог открытия файла
	if (d.DoModal() == IDOK) {
		name_file_out = d.m_ofn.lpstrFile;
		UpdateData(0);
	}
	// TODO: добавьте свой код обработчика уведомлений
}


void Clab7Dlg::OnBnClickedBtnOut()
{
	// TODO: добавьте свой код обработчика уведомлений
	CFileDialog d(0);//диалог открытия файла
	if (d.DoModal() == IDOK) {
		name_file_out = d.m_ofn.lpstrFile;
		UpdateData(0);
	}
}


void Clab7Dlg::OnBnClickedBtnDoit()
{
	UpdateData(0);
	if (!is_succed(name_file_in,name_file_out)) {
		MessageBox(L"Произошла ошибка в обработке файлов!", L"Ошибка", MB_OK | MB_ICONERROR);
	}
	else {
		ifstream f_in(name_file_in);
		string s;
		char a;
		while (!f_in.eof()) {
			f_in >> a;
			s += a;
		}
		f_in.close();
		ofstream f_out(name_file_out);
		CString str(s.c_str());
		MessageBox(str, L"TEXT", MB_OK);
		for (string::iterator i1 = s.begin(); i1 != s.end(); ++i1)
			f_out << *i1;
		f_out.close();
	}

}

BOOL Clab7Dlg::is_succed(CString fin, CString fout) 
{
	ifstream f_in(name_file_in);
	ofstream f_out(name_file_out);
	if (!f_in.is_open())
		return FALSE;
	if (!f_out.is_open())
		return FALSE;
	return TRUE;
}