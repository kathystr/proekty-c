// lab7Dlg.h: файл заголовка
//

#pragma once


// Диалоговое окно Clab7Dlg
class Clab7Dlg : public CDialogEx
{
// Создание
public:
	Clab7Dlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_LAB7_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnInFile();
	CString name_file_in;
	afx_msg void OnEnChangeEdit2();
	afx_msg void OnEnChangeEditFileout();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedBtnOut();
	CString name_file_out;
	afx_msg void OnBnClickedBtnDoit();
	BOOL is_succed(CString fin, CString fout);
};
