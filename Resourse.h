//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется lab7.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_LAB7_DIALOG                 102
#define IDR_MAINFRAME                   128
#define IDC_EDIT_IN                     1000
#define IDC_BTN_INFILE                  1001
#define IDC_EDIT_FILEOUT                1002
#define IDC_BTN_OUT                     1004
#define IDC_B                           1005
#define IDC_BTN_DOIT                    1005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
